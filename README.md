# README #

This is a survival test for maniak, was made with:

NPM gulp
SCSS - CSS3
HTML5
SVG Optimized images to reduce loading time in website

### What is this repository for? ###

* It's a simple library website
* Version 1.0.0
* [Learn Markdown](http://laboratorio-web.com.mx/)

### How do I get set up? ###

* Dependencies are in package.json
* SETUP: Install this file, it contains all dependencies nedeed, then run:
# npm start


* DEVELOPMENT ENVIRONMENT: is located in "dev" folder, ALL CHANGES MUST BE DONE THERE.
* LIVE ENVIRONMENT: is located in "build" folder.


* How to run tests: open index.html file in "build" folder
* Database configuration no needed
* Deployment instructions: 
	Run in terminal "gulp" in order to get the following files minified and compressed:
	JS
	CSS
	SVG
	JPG
	HTML


### Who do I talk to? ###

* https://bitbucket.org/radioh_/
* danielayvar@hotmail.com
* ayvarlopez@gmail.com
* http://laboratorio-web.com.mx/
* Other community or team contact: Smoke signals
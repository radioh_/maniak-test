
$(document).ready(function(){
	  	$('.bxslider').bxSlider();
});

$(".about").click(function() {
		    $('html, body').animate({
		        scrollTop: $("#aboutdiv").offset().top - 150
		    }, 1000);
		});

 function initJumpMenus() {
        var selectElements = document.getElementsByTagName("select");
        for( i = 0; i < selectElements.length; i++ ) {
            if( selectElements[i].className == "jumpmenu" && document.getElementById(selectElements[i].id) != "" ) {
                jumpmenu = document.getElementById(selectElements[i].id);
                jumpmenu.onchange = function() {
                    if( this.options[this.selectedIndex].value != '' ) {
                        if (this.options[this.selectedIndex].value == 'aboutus') {
                        	$('html, body').animate({
						        scrollTop: $("#aboutdiv").offset().top - 150
						    }, 1000);
                        }else{
                        	location.href=this.options[this.selectedIndex].value;

                        }
                    }
                }
            }
        }
    }
    window.onload = function() {
        initJumpMenus();
    }
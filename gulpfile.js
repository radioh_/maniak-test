const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const minify = require('gulp-minify-css');
const htmlmin = require('gulp-htmlmin');
const imagemin = require('gulp-imagemin');


gulp.task('js', function(){
   gulp.src('dev/js/*.js')
   .pipe(concat('script-main.js'))
   .pipe(uglify())
   .pipe(gulp.dest('build/js/'));
});

gulp.task('css', function(){
   gulp.src('dev/css/*.css')
   .pipe(concat('main.css'))
   .pipe(minify())
   .pipe(gulp.dest('build/css/'));
});

gulp.task('html', function(){
   gulp.src('dev/*.html')
  .pipe(htmlmin({collapseWhitespace: true}))
  .pipe(gulp.dest('build'));
});

gulp.task('image', () =>
    gulp.src('dev/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/img'))
);

gulp.task('default',['js','css', 'html', 'image'],function(){
});